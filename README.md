# Common deployment scripts used in gitlab CI/CD

This is to circumvent issue https://gitlab.com/gitlab-org/gitlab-ee/issues/5493 until it is fixed

Folder structure:

`/scripts/<type>/v1`:
    - type is to identify the type of script example: go, python, shell etc
    - versioning allows us to use same branch with multiple repositories if required

List of scripts and what they do:
* `shell/process_env_to_k8s_variables.sh`: Processes k8s deployment templates with environment variables set in gitlab CI/CD to prepare final yaml
