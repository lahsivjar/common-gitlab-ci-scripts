#!/bin/bash

process_template() {
  if [[ -z "$1" ]]; then
    echo >&2 "File name of k8s template is required"
    exit 1
  fi

  echo "|--Processing template $1..."

  local filename=$1
  local deploy_file=${filename%.*}

  move_template $1 $deploy_file

  replace_variable $deploy_file GCP_PROJECT_ID $GCP_PROJECT_ID
  replace_variable $deploy_file GCP_REGION $GCP_REGION
  replace_variable $deploy_file DEPLOY_VERSION $CI_COMMIT_REF_NAME
  replace_variable $deploy_file FILE_STORAGE_BUCKET $FILE_STORAGE_BUCKET
  replace_variable $deploy_file PUBLIC_BUCKET $PUBLIC_BUCKET
  replace_variable $deploy_file DEFAULT_REPLICAS $DEFAULT_REPLICAS
  replace_variable $deploy_file SQL_CLUSTER_REGION $SQL_CLUSTER_REGION
  replace_variable $deploy_file SQL_INSTANCE_ID $SQL_INSTANCE_ID
  replace_variable $deploy_file CI_COMMIT_SHA $CI_COMMIT_SHA
  replace_variable $deploy_file CI_JOB_ID $CI_JOB_ID
  replace_variable $deploy_file DOMAIN $DOMAIN
  replace_variable $deploy_file WILDCARD_DOMAIN $WILDCARD_DOMAIN
  replace_variable $deploy_file REDIS_HOST $REDIS_HOST
  replace_variable $deploy_file REDIS_PORT $REDIS_PORT
  replace_variable $deploy_file ES_HOST $ES_HOST
  replace_variable $deploy_file ES_PORT $ES_PORT
}

move_template() {
  echo "  |--Moving file $1 to $2..."
  mv $1 $2
}

replace_variable() {
  echo "  |--Replacing variable {{ $2 }} with $3..."
  sed -i.bak "s/{{ $2 }}/$3/g" $deploy_file
}

apply() {
  while read line; do
    process_template $line
  done < <(find kubernetes -name '*.template')
}

apply
